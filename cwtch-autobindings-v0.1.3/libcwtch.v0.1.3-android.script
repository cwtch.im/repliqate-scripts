# Mount Virtual Disk for More Space
@!setup-secondary

apt update -qq
apt install -qq -y --no-install-recommends strace netcat git wget libc6-dev make gcc unzip ca-certificates p11-kit
@%uname -a
@%ldd --version
export LANG=C.UTF-8

# Install JDK8
cd /mount
@%wget https://github.com/AdoptOpenJDK/openjdk8-upstream-binaries/releases/download/jdk8u342-b07/OpenJDK8U-jdk_x64_linux_8u342b07.tar.gz
@!check OpenJDK8U-jdk_x64_linux_8u342b07.tar.gz 9a871e4dd2698ae8f4063aad7998414a7472926fbe46b3c89567d86338427bb85acb7e390e88c3c311cd5e631051b358530352fc6e7d65e9ea6c8cff0e72bc09
tar --extract --file OpenJDK8U-jdk_x64_linux_8u342b07.tar.gz --no-same-owner
ln -s /mount/openjdk-8u342-b07 /usr/local/openjdk-8
export JAVA_HOME=/usr/local/openjdk-8
export PATH="/usr/local/openjdk-8/bin/:$PATH"
@%find "$JAVA_HOME/lib" -name '*.so' -exec dirname '{}' ';' | sort -u > /etc/ld.so.conf.d/docker-openjdk.conf
@%ldconfig
@%trust extract --overwrite --format=java-cacerts --filter=ca-anchors --purpose=server-auth "$JAVA_HOME/jre/lib/security/cacerts"
@%java -version

# Setup Android SDK
mkdir -p /usr/local/android-sdk
cd /usr/local/android-sdk
mkdir .android
wget -q -O sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
unzip -q sdk.zip
rm sdk.zip
export ANDROID_SDK=/usr/local/android-sdk
export ANDROID_HOME=/usr/local/android-sdk
yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses
$ANDROID_HOME/tools/bin/sdkmanager --update
$ANDROID_HOME/tools/bin/sdkmanager "build-tools;30.0.2" "platforms;android-31" "platform-tools"
$ANDROID_HOME/tools/bin/sdkmanager "ndk;22.1.7171670"
export NDK_VER="22.1.7171670"
export ANDROID_NDK_HOME=/usr/local/android-sdk/ndk/$NDK_VER
@%ln -sf $ANDROID_HOME/ndk/$NDK_VER $ANDROID_HOME/ndk-bundle
echo 'hosts: files dns' > /etc/nsswitch.conf



# Setup Go
cd /mount
mkdir gopath
# Downloading Go
wget https://go.dev/dl/go1.23.0.linux-amd64.tar.gz
@!check go1.23.0.linux-amd64.tar.gz 4bab785fc2ec027e78d018fb0b800349f8d6110fd3a786ef35219b12411baa266683bfde4f78565c423f9e5db0b16f1b24900697ed2c1741767bbe583b3d0bc9
tar -xzf go1.23.0.linux-amd64.tar.gz

ln -s /mount/go /usr/local/go
ln -s /mount/gopath /gomobile
export GOROOT="/usr/local/go"
export GOPATH="/gomobile"
export PATH="$GOPATH/bin:/usr/local/go/bin:$PATH"
go version


# Building gomobile - NOTE this hash is important as it binds to Java 1.7 class support
# We can't update this until docker is updated
export GOMOBILEHASH="43a0384520996c8376bfb8637390f12b44773e65"
mkdir -p $GOPATH/src/golang.org/x
mkdir -p $GOPATH/bin/
mkdir -p $GOPATH/pkg/
cd $GOPATH/src/golang.org/x
git clone https://github.com/golang/mobile.git
cd mobile
git checkout $GOMOBILEHASH
#go install -trimpath -ldflags "-buildid=" golang.org/x/mobile/cmd/gomobile@$GOMOBILEHASH
#go install -trimpath -ldflags "-buildid=" golang.org/x/mobile/cmd/gobind@$GOMOBILEHASH
go install ./cmd/gobind
go install ./cmd/gomobile
@%gomobile version
@%go env
gomobile clean

# Building libCwtch.so
cd /mount
git clone https://git.openprivacy.ca/cwtch.im/autobindings
mkdir /drone
ln -s /mount/autobindings /drone/src
cd /drone/src
git fetch --tags
git checkout tags/v0.1.3 -b v0.1.3
echo `git describe --tags` > VERSION
make clean-autobindings
env EXPERIMENTS="" make lib.go
go mod download
gomobile init
@%env
@%make android
sha512sum build/android/cwtch.aar
@!extract build/android/cwtch.aar
@!check build/android/cwtch.aar 50bb86c038a9fdf294993bf98c622507f5d78c4d8dc5a78ce27e7ab9a85703faff47a819a76cc4ce44d2f47236b8a7aadbb5a094da735313e03cdb11a4cb88d0
